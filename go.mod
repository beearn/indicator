module gitlab.com/beearn/indicator

go 1.19

require (
	github.com/cinar/indicator v1.2.25
	gitlab.com/beearn/entity v1.0.0
)
