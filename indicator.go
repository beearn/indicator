package indicator

import (
	"github.com/cinar/indicator"
	"gitlab.com/beearn/entity"
	"strconv"
)

type Lines struct {
	high    []float64
	low     []float64
	closing []float64
}

func (t *Lines) StochPrice() ([]float64, []float64) {
	k, d := indicator.StochasticOscillator(t.high, t.low, t.closing)

	return k, d
}

func (t *Lines) RSI(period int) ([]float64, []float64) {
	rs, rsi := indicator.RsiPeriod(period, t.closing)

	return rs, rsi
}

func (t *Lines) StochRSI(rsiPeriod int) ([]float64, []float64) {
	_, rsi := t.RSI(rsiPeriod)
	k, d := indicator.StochasticOscillator(rsi, rsi, rsi)

	return k, d
}

func Smooth(period int, values []float64) []float64 {
	return indicator.Sma(period, values)
}

func SmoothEMA(period int, values []float64) []float64 {
	return indicator.Ema(period, values)
}

func (t *Lines) SMA(period int) []float64 {
	return indicator.Sma(period, t.closing)
}

func (t *Lines) MACD() ([]float64, []float64) {
	return indicator.Macd(t.closing)
}

func (t *Lines) EMA(periods ...int) [][]float64 {
	var res [][]float64
	for i := range periods {
		res = append(res, indicator.Ema(periods[i], t.closing))
	}

	return res
}

func LoadKlines(klines []*entity.Kline) *Lines {
	t := &Lines{}
	for i := range klines {
		closePrice, _ := strconv.ParseFloat(klines[i].Close, 64)
		t.closing = append(t.closing, closePrice)
		lowPrice, _ := strconv.ParseFloat(klines[i].Low, 64)
		t.low = append(t.low, lowPrice)
		highPrice, _ := strconv.ParseFloat(klines[i].High, 64)
		t.high = append(t.high, highPrice)
	}

	return t
}
